# B-Tree layout

В этой задаче необходимо реализовать layout внутренних узлов BTreeInternalPage и листовых узлов BTreeLeafPage в `btree_page.h` и `btree_page.cpp`.

Для сравнения строк реализуйте `Comparator` в `comparator.cpp`.

В результате должен работать тест `btree_1_layout.cpp`.
