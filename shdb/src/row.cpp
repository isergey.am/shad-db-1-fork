#include "row.h"

#include <sstream>

namespace shdb
{

std::string toString(const Row & row)
{
    std::stringstream stream;
    stream << '[';
    for (size_t index = 0; index < row.size(); ++index)
    {
        const auto & value = row[index];
        std::visit(
            Overloaded{
                [&](const Null &) { stream << "NULL"; },
                [&](const uint64_t & value) { stream << value; },
                [&](const bool & value) { stream << (value ? "true" : "false"); },
                [&](const std::string & value) { stream << value; },
            },
            value);
        if (index < row.size() - 1)
            stream << ',';
    }
    stream << ']';
    return stream.str();
}

std::string toString(const RowId & row_id)
{
    std::stringstream stream;
    stream << '(';
    stream << row_id.page_index;
    stream << ',';
    stream << row_id.row_index;
    stream << ')';

    return stream.str();
}

std::ostream & operator<<(std::ostream & stream, const Row & row)
{
    stream << toString(row);
    return stream;
}

std::ostream & operator<<(std::ostream & stream, const RowId & row_id)
{
    stream << toString(row_id);
    return stream;
}

bool RowId::operator==(const RowId & other) const
{
    return page_index == other.page_index && row_index == other.row_index;
}

bool RowId::operator!=(const RowId & other) const
{
    return !(*this == other);
}

}
