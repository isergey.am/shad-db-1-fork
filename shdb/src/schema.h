#pragma once

#include <memory>
#include <string>
#include <tuple>
#include <vector>

namespace shdb
{

enum class Type
{
    boolean,
    uint64,
    varchar,
    string,
};

struct ColumnSchema
{
    ColumnSchema(std::string name_, Type type_, uint32_t length_ = 0) : name(std::move(name_)), type(type_), length(length_) { }

    ColumnSchema(Type type_, uint32_t length_ = 0) : type(type_), length(length_) { } /// NOLINT

    ColumnSchema() = default;

    std::string name;
    Type type;
    uint32_t length;

    bool operator==(const ColumnSchema & other) const
    {
        return std::tie(name, type, length) == std::tie(other.name, other.type, other.length);
    }
};

using Schema = std::vector<ColumnSchema>;
using SchemePtr = std::shared_ptr<Schema>;

static SchemePtr createSchema(const std::vector<ColumnSchema> & column_schemas)
{
    return std::make_shared<Schema>(column_schemas);
}

static SchemePtr createSchema(std::vector<ColumnSchema> && column_schemas)
{
    return std::make_shared<Schema>(std::move(column_schemas));
}
}
