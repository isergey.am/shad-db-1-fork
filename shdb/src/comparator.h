#pragma once

#include "row.h"

namespace shdb
{

struct Comparator
{
    std::strong_ordering operator()(const Row & lhs, const Row & rhs) const;
};

/// Compare rows using Comparator
std::strong_ordering compareRows(const Row & lhs, const Row & rhs);

}
