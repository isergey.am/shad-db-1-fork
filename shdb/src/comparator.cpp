#include "comparator.h"

#include <cassert>
#include <cstdint>
#include <string>
#include <variant>

namespace shdb
{

std::strong_ordering Comparator::operator()(const Row & lhs, const Row & rhs) const
{
    (void)(lhs);
    (void)(rhs);
    throw std::runtime_error("Not implemented");
}

std::strong_ordering compareRows(const Row & lhs, const Row & rhs)
{
    return Comparator()(lhs, rhs);
}

}
