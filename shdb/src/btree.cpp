#include "btree.h"
#include "btree_page.h"

#include <cassert>

namespace shdb
{

BTree::BTree(const IndexMetadata & metadata_, Store & store_, std::optional<size_t> page_max_keys_size)
    : IIndex(metadata_), metadata_page(nullptr)
{
    if (!page_max_keys_size)
    {
        size_t internal_page_max_keys_size = BTreeInternalPage::calculateMaxKeysSize(metadata.fixedKeySizeInBytes());
        size_t leaf_page_max_keys_size = BTreeLeafPage::calculateMaxKeysSize(metadata.fixedKeySizeInBytes());
        page_max_keys_size = std::min(internal_page_max_keys_size, leaf_page_max_keys_size);
    }

    max_page_size = *page_max_keys_size;
    auto page_provider = createBTreePageProvider(metadata.getKeyMarshal(), metadata.fixedKeySizeInBytes(), max_page_size);
    index_table.setIndexTable(store_.createOrOpenIndexTable(metadata.getIndexName(), page_provider));

    bool initial_index_creation = index_table.getPageCount() == 0;

    if (initial_index_creation)
    {
        auto [allocated_metadata_page, metadata_page_index] = index_table.allocateMetadataPage();
        assert(metadata_page_index == MetadataPageIndex);

        auto [root_page, root_page_index] = index_table.allocateLeafPage();

        root_page.setPreviousPageIndex(InvalidPageIndex);
        root_page.setNextPageIndex(InvalidPageIndex);

        metadata_page = std::move(allocated_metadata_page);
        metadata_page.setRootPageIndex(root_page_index);
        metadata_page.setMaxPageSize(max_page_size);
        metadata_page.setKeySizeInBytes(metadata.fixedKeySizeInBytes());
        return;
    }

    metadata_page = index_table.getMetadataPage(MetadataPageIndex);

    if (metadata.fixedKeySizeInBytes() != metadata_page.getKeySizeInBytes())
        throw std::runtime_error(
            "BTree index inconsistency. Expected " + std::to_string(metadata_page.getKeySizeInBytes()) + " key size in bytes. Actual "
            + std::to_string(metadata.fixedKeySizeInBytes()));

    if (max_page_size != metadata_page.getMaxPageSize())
        throw std::runtime_error(
            "BTree index inconsistency. Expected " + std::to_string(metadata_page.getMaxPageSize()) + " max page size. Actual "
            + std::to_string(max_page_size));
}

void BTree::insert(const IndexKey & index_key, const RowId & row_id)
{
    (void)(index_key);
    (void)(row_id);
    throw std::runtime_error("Not implemented");
}

bool BTree::remove(const IndexKey & index_key, const RowId &)
{
    (void)(index_key);
    throw std::runtime_error("Not implemented");
}

void BTree::lookup(const IndexKey & index_key, std::vector<RowId> & result)
{
    (void)(index_key);
    (void)(result);
    throw std::runtime_error("Not implemented");
}

namespace
{

class BTreeEmptyIndexIterator : public IIndexIterator
{
public:
    std::optional<std::pair<IndexKey, RowId>> nextRow() override { return {}; }
};

class BTreeIndexIterator : public IIndexIterator
{
public:
    explicit BTreeIndexIterator(
        BTreeIndexTable & index_table_,
        BTreeLeafPage leaf_page_,
        size_t leaf_page_offset_,
        std::optional<Row> max_key,
        bool include_max_key_ = false)
        : index_table(index_table_)
        , leaf_page(leaf_page_)
        , leaf_page_offset(leaf_page_offset_)
        , max_key(std::move(max_key))
        , include_max_key(include_max_key_){};

    std::optional<std::pair<IndexKey, RowId>> nextRow() override { throw std::runtime_error("Not implemented"); }

private:
    bool isRowValid(const Row & key)
    {
        (void)(key);
        throw std::runtime_error("Not implemented");
    }

    BTreeIndexTable & index_table;
    BTreeLeafPage leaf_page;
    size_t leaf_page_offset;
    std::optional<Row> max_key;
    bool include_max_key;
    Row previous_row;
};

}

std::unique_ptr<IIndexIterator> BTree::read()
{
    throw std::runtime_error("Not implemented");
}

std::unique_ptr<IIndexIterator> BTree::read(const KeyConditions & predicates)
{
    (void)(predicates);
    throw std::runtime_error("Not implemented");
}

void BTree::dump(std::ostream & stream)
{
    PageIndex pages_count = index_table.getPageCount();
    for (PageIndex i = 0; i < pages_count; ++i)
    {
        auto page = index_table.getPage(i);
        auto page_type = page->getPageType();

        stream << "Page " << i << " page type " << toString(page_type) << '\n';

        switch (page_type)
        {
            case BTreePageType::invalid: {
                break;
            }
            case BTreePageType::metadata: {
                auto metadata_page = BTreeMetadataPage(page);
                metadata_page.dump(stream);
                break;
            }
            case BTreePageType::internal: {
                auto internal_page = BTreeInternalPage(page);
                internal_page.dump(stream);
                break;
            }
            case BTreePageType::leaf: {
                auto leaf_page = BTreeLeafPage(page);
                leaf_page.dump(stream);
                break;
            }
        }
    }
}

BTreeLeafPage BTree::lookupLeafPage(const IndexKey & index_key)
{
    (void)(index_key);
    throw std::runtime_error("Not implemented");
}

BTreeLeafPage BTree::lookupLeftmostLeafPage()
{
    throw std::runtime_error("Not implemented");
}

BTreePtr BTree::createIndex(const IndexMetadata & index_metadata, Store & store)
{
    return std::shared_ptr<BTree>(new BTree(index_metadata, store, {}));
}

BTreePtr BTree::createIndex(const IndexMetadata & index_metadata, size_t page_max_keys_size, Store & store)
{
    return std::shared_ptr<BTree>(new BTree(index_metadata, store, page_max_keys_size));
}

void BTree::removeIndex(const std::string & name_, Store & store)
{
    store.removeTable(name_);
}

void BTree::removeIndexIfExists(const std::string & name_, Store & store)
{
    store.removeTableIfExists(name_);
}

}
